import { Injectable, group } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
// import { AngularFireAuth } from 'angularfire2/auth';
// import { Observable, from } from 'rxjs';
import * as firebase from 'firebase';
import { async } from 'rxjs/internal/scheduler/async';

import { UserService } from './user.service';

@Injectable()
export class UserLogService {

    public group_check_list: any;

constructor(
    public firestore: AngularFirestore,
    public userService: UserService
) {}

    registerUserLog(user_id, group_event_id, flag, meta, group_avatar) {

        var log = {
            user_id: user_id,
            flag: flag,
            group_event_id: group_event_id,
            meta: meta,
            group_avatar: group_avatar,
            created_at: new Date().getTime()
        }
        
        return this.firestore.collection("user_log").add(log);
    }

    getUserLog(user_id) {
        var promise = new Promise((resolve, reject) => {
            this.firestore.collection("user_log").ref.where("user_id", "==", user_id).orderBy("flag").where("flag", "<", 4).orderBy("created_at").limit(5).get().then((snapshot) => {
                var data = [];

                snapshot.forEach(function(doc) {
                    data.push(doc.data());
                });

                resolve(data);
            }).catch((err) => {
              reject(err);
            })
        });
    
        return promise;
    }

    getNotification() {
        var promise = new Promise((resolve, reject) => {
            //var todayStart = Math.floor(new Date().getTime() / 86400000) * 86400000;
            
            this.firestore.collection("user_log").ref.orderBy("flag").where("flag", ">", 2).where("flag", "<", 5).orderBy("created_at").get().then((snapshot) => {
                var data = [];

                snapshot.forEach(function(doc) {

                    //if (doc.data().created_at > todayStart)
                        data.push(doc.data());

                });

                resolve(data);
            }).catch((err) => {
              reject(err);
            })
        });
    
        return promise;
    }

    getPosts() {
        var promise = new Promise((resolve, reject) => {
            //var todayStart = Math.floor(new Date().getTime() / 86400000) * 86400000;
            
            this.firestore.collection("user_log").ref.where("flag", "==", 5).orderBy("created_at").get().then((snapshot) => {
                var data = [];

                var that = this;

                snapshot.forEach( async function(doc) {
                    var item = {};

                    var user = await that.userService.getAsyncUser(doc.data().user_id);

                    item['user_name'] = user.name.toUpperCase();
                    item['user_avatar'] = user.avatar;
                    item['post'] = doc.data().meta.description;
                    item['group_event_id'] = doc.data().group_event_id;
                    item['created_at'] = new Date(doc.data().created_at).toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric'});

                    //if (doc.data().created_at > todayStart)
                        data.push(item);

                });

                resolve(data);
            }).catch((err) => {
              reject(err);
            })
        });
    
        return promise;
    }

    async getGroupPosts(group_id) {
        var posts = [];

        var snapshot = await this.firestore.collection("user_log").ref.where("group_event_id", "==", group_id).where("flag", "==", 5).orderBy('created_at').get();

        snapshot.docs.forEach(async (doc) => {
            var item = {};

            var snapshot1 = await this.firestore.collection("users").doc(doc.data()['user_id']).ref.get();

            item['user_name'] = snapshot1.data()['name'].toUpperCase();
            item['user_avatar'] = snapshot1.data()['avatar'];
            item['post_text'] = doc.data()['meta']['description'];
            item['created_at'] = doc.data()['created_at'];

            posts.push(item);
        });

        return posts;
    }
}

// - flag :
//     0 - create a group,
//     1 - request to join
//     2 - joined to group
//     3 - add media to group
//     4 - create event
//     5 - create group post