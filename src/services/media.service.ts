import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class MediaService {

    public mediaList = [];

constructor(
    public firestore: AngularFirestore
) {}
    async getList(idList) {
        var returnData = [];

        for (var i = 0; i < idList.length; i ++) {
            var snapshot = await this.firestore.collection("media").doc(idList[i]).ref.get();

            returnData.push(snapshot.data());
        }

        return returnData;
    }

    uploadMedia(media) {
        return this.firestore.collection("media").add(media);
    }
}