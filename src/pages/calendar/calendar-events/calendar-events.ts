import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-calendar-events',
  templateUrl: 'calendar-events.html',
})
export class CalendarEventsPage {
  eventList = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams
  ) {
    this.eventList = navParams.get('eventList');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarEventsPage');
  }

}
