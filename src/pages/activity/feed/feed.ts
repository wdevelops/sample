import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { UserLogService } from '../../../services/user_log.service';

@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {
  activityList: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController,
    public modal: ModalController,
    public userLogService: UserLogService
  ) {
    this.activityList = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityPage');
  }

  ionViewWillEnter() {
    const loading = this.loadingCtrl.create();

    this.userLogService.getNotification().then((notifications) => {
      this.activityList = notifications;
  
        loading.dismiss();
    });

    loading.present();
  }

}
