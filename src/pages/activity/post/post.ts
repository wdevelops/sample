import { Component } from '@angular/core';
import {NavController, NavParams, ViewController, LoadingController, ModalController, ModalOptions, Modal } from 'ionic-angular';
import { UserLogService } from '../../../services/user_log.service';
import { GroupService } from '../../../services/group.service';
import { FeedPage } from '../feed/feed';

@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  activityList: any;
  myModal: Modal;
  showModal = true;
  groupList: any;
  allActivityList: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    private groupService: GroupService,
    public modal: ModalController,
    public userLogService: UserLogService
  ) {
    this.groupList = [];
    this.allActivityList = [];
    this.activityList = [];
    this.viewCtrl = this.navParams.get('viewCtrl');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityPage');
  }

  triggerModal() {
    if (this.showModal) {
      this.openModal();
    } else {
      this.closeModal();
    }
  }

  openModal() {

    this.showModal = false;

    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false,
      cssClass: 'custom-modal'
    };

    this.myModal = this.modal.create('ModalPage', { data: this.groupList }, myModalOptions);

    this.myModal.present();

    this.myModal.onDidDismiss((data) => {
      console.log("I have dismissed.");
      console.log(data);
    });

    this.myModal.onWillDismiss((data) => {
      console.log("I'm about to dismiss");
      console.log(data);
    });

  }

  closeModal() {
    if (!this.showModal) {
      this.showModal = true;

      this.filterGroups();

      this.myModal.dismiss();
    }
  }

  ionViewWillEnter() {
    const loading = this.loadingCtrl.create();

    this.userLogService.getPosts().then((notifications) => {
      this.allActivityList = notifications;

      this.groupService.getAllGroups().then((res) => {
        this.groupList = res;

        this.initCheckList();
        this.filterGroups();
  
        loading.dismiss();
      });
    });

    loading.present();
  }

  filterGroups() {
    this.activityList = [];

      for (var i = 0; i < this.allActivityList.length; i ++) {

          if(this.userLogService.group_check_list[this.allActivityList[i].group_event_id]) {

            this.activityList.push(this.allActivityList[i]);
  
          }

      }
  }

  initCheckList() {

    if (!this.userLogService.group_check_list)
      this.userLogService.group_check_list = [];

    for(var i = 0; i < this.groupList.length; i ++) {

      if (this.userLogService.group_check_list[this.groupList[i].id] === undefined) {
        this.userLogService.group_check_list[this.groupList[i].id] = true;
      }

    }
  }

  gotoNotifications() {
    this.navCtrl.push(FeedPage);
  }

}
