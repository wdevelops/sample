import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { MorePage } from '../../more/more';

@Component({
  selector: 'page-media-detail',
  templateUrl: 'media-detail.html',
})
export class MediaDetailPage {
  media: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
    this.media = navParams.get('media');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MediaDetailPage');
  }

  more(ev) {
    let popover = this.popoverCtrl.create(MorePage, {
      data: 'mediaDetail'
    });
    popover.present({
      ev: ev
    });
  }

}
